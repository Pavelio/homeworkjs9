window.onload = function () {
   document.getElementById('tabs').addEventListener('click', activeTabs);

   function activeTabs(event) {
      if(event.target.className == "tabs-title") {
         let dataTab = event.target.getAttribute('data-tab');
         let tabTitle = document.getElementsByClassName('tabs-title');
         for( let i=0; i<tabTitle.length; i++) {
            tabTitle[i].classList.remove('active')
         }
         tabTitle[dataTab].classList.add('active');

         let tabOpacity = document.getElementsByClassName('tab-second');
         for( let i=0; i<tabOpacity.length; i++) {
            if(dataTab==i) {
               tabOpacity[i].style.display = "block";
            }
            else {
               tabOpacity[i].style.display = "none";
            }
         }
      }
   }
}